--- Required libraries.

-- Localised functions.
local find = string.find
local lower = string.lower
local random = math.random
local time = os.time
local cancel = transition.cancel
local pause = transition.pause
local resume = transition.resume

-- Localised values.

--- Class creation.
local library = {}

-- Static values.

--- Initialises this Scrappy library.
function library:init()

	-- Table to store named transitions
	self._transitions = {}

	-- Loop through the transition library
	for k, v in pairs( transition ) do

		-- Check if this item is a function, and it isn't a private one, nor a 'sequence' one ( because I don't know what those are ), nor is it one we already have, nor the enterFrame handler.
		if type( v ) == "function" and not find( k, "_" ) and not self[ k ] and not find( lower( k ), "sequence" ) and k ~= "enterFrame" then

			-- Create a wrapper for it
			self[ k ] = function( self, object, params, name )

				-- And call our new transition method, making sure to return the transition name
				return Scrappy.Transition:new( object, params, k, name )

			end

		end

	end

end

--- Creates a new transition.
-- @param object The object to apply the transition to.
-- @param params The transition paramaters.
-- @param method The method to use, such as 'to' and 'from'. Defaults to 'to'.
-- @param name The name of the new transition. Optional, defaults to 'transition' followed by a random number.
-- @return The name of the new transition.
function library:new( object, params, method, name )

	-- Check if we actually have a transition method
	if transition[ method or "to" ] then

		-- Do we have a name? If not, create a new one
		if not name then

			-- Function to create a new name
			local newName = function()
				return "transition" .. random( time() )
			end

			-- Create the new name
			name = newName()

			-- Does it already exist?
			while self:get( name ) do

				-- Get another name
				name = newName()

			end

		end

		-- Cancel and existing transition with this name
		self:cancel( name )

		-- Create the new transition and store it
		self._transitions[ name ] = transition[ method or "to" ]( object, params )

		-- Return the name
		return name

	end

end


--- Pauses a transition.
-- @param name The name of the transition. Optional, if left out all named transitions will be paused.
-- @param isTag True if the name you are passing in is actually a transition tag. Optional, defaults to false.
function library:pause( name, isTag )

	-- Do we have a name?
	if name then

		-- Was the name a string?
		if type( name ) == "string" then

			-- Pause it
			pause( isTag and name or self:get( name ) )

		-- Wasn't a string
		else

			-- So try to pause it
			pause( name )

		end

	-- No name passed in
	else

		-- So loop through all transitions
		for k, v in pairs( self._transitions ) do

			-- And pause them
			self:pause( k )

		end

	end

end

--- Resumes a transition.
-- @param name The name of the transition. Optional, if left out all named transitions will be resumed.
-- @param isTag True if the name you are passing in is actually a transition tag. Optional, defaults to false.
function library:resume( name, isTag )

	-- Do we have a name?
	if name then

		-- Was the name a string?
		if type( name ) == "string" then

			-- Resume it
			resume( isTag and name or self:get( name ) )

		-- Wasn't a string
		else

			-- So try to resume it
			resume( name )

		end

	-- No name passed in
	else

		-- So loop through all transitions
		for k, v in pairs( self._transitions ) do

			-- And resume them
			self:resume( k )

		end

	end

end

--- Cancels a transition.
-- @param name The name of the transition. Optional, if left out all named transitions will be cancelled.
-- @param isTag True if the name you are passing in is actually a transition tag. Optional, defaults to false.
function library:cancel( name, isTag )

	-- Do we have a name?
	if name then

		-- Was the name a string?
		if type( name ) == "string" then

			-- Was the name a tag?
			if isTag then

				-- Cancel it
				cancel( name )

			-- Otherwise, do we have a transition with this name?
			elseif self:get( name ) then

				-- Cancel it
				cancel( self:get( name ) )

			end

			-- If it wasn't a tag
			if not isTag then

				-- Then also remove it
				self:remove( name )

			end

		-- Wasn't a string
		else

			-- So try to cancel it
			cancel( name )

		end

	-- No name passed in
	else

		-- So loop through all transitions
		for k, v in pairs( self._transitions ) do

			-- And cancel them
			self:cancel( k )

		end

	end

end


--- Removes a transition.
-- @param name The name of the transition. Optional, if left out all named transitions will be removed.
function library:remove( name )

	-- Do we have a name?
	if name then

		-- Remove it
		self._transitions[ name ] = nil

	-- No name passed in
	else

		-- Loop through all transitions
		for k, v in pairs( self._transitions ) do

			-- And remove them
			self:remove( k )

		end

	end

end

--- Gets a transition reference.
-- @param name The name of the transition.
-- @return The transition reference, nil if none found.
function library:get( name )
	return self._transitions[ name ]
end

--- Counts all named transitions.
-- @return The total count.
function library:count()

	local count = 0

	for _, _ in pairs( self._transitions ) do
		count = count + 1
	end

	return count

end


--- Destroys this Scrappy Library.
function library:destroy()

	-- Cancel all transitions
	self:cancel()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Transition library
if not Scrappy.Transition then

	-- Then store the library out
	Scrappy.Transition = library

end

-- Return the new library
return library
